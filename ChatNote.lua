
--============================================================================--
-- Local functions
--============================================================================--

---
-- Retrieves guild note for the character with a given GUID. This function only works if GuildRoster() has
-- been invoked and the GUILD_ROSTER_UPDATE event has subsequently fired.
-- @param characterGuid (string) GUID of the character
-- @return (string) The guild note set for the specified character
local function GetGuildNote(characterGuid)

	-- Don't bother looking if no GUID was provided
	if (not characterGuid) then
		return;
	end


	-- Get number of guild members
	local numGuildMembers = GetNumGuildMembers();

	-- Check each guild roster index for matching player
	for i=1,numGuildMembers do

		-- Get guild roster info for index
		local fullName, _, _, _, class, _, note = GetGuildRosterInfo(i);

		-- Get author name and realm
		local _, _, _, _, _, _, name, realm = pcall(GetPlayerInfoByGUID, characterGuid);

		-- If no realm returned, use player's realm
		if (not realm or realm == "") then
			_, realm = UnitFullName("PLAYER");
		end

		-- If current guild member matches the character, return the guild note 
		if (name and realm) then
			local nameAndRealm = name.."-"..gsub(realm,"[%s%-]","");
			if (fullName == nameAndRealm) then
				return note;
			end
		end
	end
end


---
-- Searches the guild roster for a character matching the guild note and returns that character's note. 
-- If multiple characters are found with matching names (on different realms), a character on the same realm
-- as the author will be preferred.
-- @param characterName (string) Character name to search for (without realm)
-- @param authorGuid (string) GUID of the author of the chat message in which the character name was found
-- @return (string) Class identifer of the matched character (referred to as 'classFilename' in Blizzard APIs)
local function GetGuildMemberClass(characterName, authorGuid)
	local bestClassMatch;

	-- Get number of guild members
	local numGuildMembers = GetNumGuildMembers();

	-- Check each guild roster index for matching player
	for i=1,numGuildMembers do

		-- Get guild roster info for index
		local fullName, _, _, _, _, _, _, _, _, _, class = GetGuildRosterInfo(i);

		-- Separate name and realm name
		local name, realm = string.match(fullName, "(.*)-(.*)");


		-- Verify current guild member name matches target name
		if (name == characterName) then

			-- Store class in case a name and realm match aren't found together
			bestClassMatch = class;

			-- Get author name and realm
			local _, _, _, _, _, _, _, authorRealm = pcall(GetPlayerInfoByGUID, authorGuid);

			-- If no author realm returned, use player's realm
			local targetRealm = (authorRealm ~= "" and authorRealm or select(2, UnitFullName("PLAYER")));

			-- If target realm matches current guild member's realm, assume that's the proper character
			if (realm == gsub(targetRealm, "[%s%-]", "")) then
				return class;
			end
		end
	end

	-- No matching name was found with matching realm, so return class from matching name only
	return bestClassMatch;
end


---
-- Colorizes a given string with a given color table. The given string will be surrounded with
-- required escape sequences to set and reset the text color.
-- @param str (string) String to colorize
-- @param color (table) Table color representing the desired color (ex. [r = 1, g = 0.2, b = 0.5])
-- @return (string) A colorized version of the given string
local function Colorize(str, color)
	str = tostring(str);
	local red = (color.r or 1) * 255;
	local green = (color.g or 1) * 255;
	local blue = (color.b or 1) * 255;
	local alpha = (color.a or 1) * 255;

	return string.format("|c%.2X%.2X%.2X%.2X%s|r", alpha, red, green, blue, str);
end


local function AddNoteToName(name, note)

	-- Ensure the note hasn't already been added
	if (string.match(name, ".*%] %[" .. note .. ".*")) then
		return name;
	end

	return name .. "] [" .. note;
end


local function AddNoteToEmoteName(name, note)

	-- Ensure the note hasn't already been added
	if (string.match(name, ".* %(" .. note .. ").*")) then
		return name;
	end

	return name .. " (" .. note .. ")";
end



--============================================================================--
-- Initialization
--============================================================================--

-- Load AceHook
local aceHook = LibStub("AceHook-3.0");

-- Initialize guild roster data
GuildRoster();


-- Hook GetColoredName in order to add player note
aceHook:RawHook("GetColoredName", function(event, ...)
	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17 = ...;
	local chatText = arg1;
	local authorGuid = arg12;

	-- Invoke original function to process 
	local coloredName = aceHook.hooks.GetColoredName(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14);


	-- Get guild note
	local note = GetGuildNote(authorGuid);


	-- If there's no colored name or note, we're done
	if (not coloredName or not note or note == "") then
		return coloredName;
	end


	-- Determine if class coloring is enabled by looking for color escape
	local classColorEnabled = string.match(coloredName, "|c.*");

	-- Attempt to color note with class color
	if (note and classColorEnabled) then

		-- Search guild roster for class of character matching the note
		local noteClass = GetGuildMemberClass(note, authorGuid);

		-- If class found for note, find the class color table
		if (noteClass) then
			local noteColor = CUSTOM_CLASS_COLORS and CUSTOM_CLASS_COLORS[noteClass] or RAID_CLASS_COLORS[noteClass];

			-- If note color was successfuly created, colorize the note
			if (noteColor) then
				note = Colorize(note, noteColor);
			end
		end
	end


	-- Handle emotes differently (there aren't square brackets)
	if (event == "CHAT_MSG_TEXT_EMOTE") then
		return AddNoteToEmoteName(coloredName, note);
	end

	-- Add guild note and return
	return AddNoteToName(coloredName, note);
end, true);