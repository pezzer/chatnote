## Interface: 70100
## Title: Chat Note
## Notes: Displays guild note in chat messages authored by guild members
## Author: Pezzer
## Version: 1.5.1

embeds.xml

ChatNote.lua